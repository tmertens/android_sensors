package de.rwth_aachen.sensor;

/**
 * Class represents a point with only a single coordinate
 * Created by admin on 15.05.2015.
 */
class Point1D {

    private final float x;

    /**
     * Default constructor
     * @param x x-coordinate
     */
    public Point1D(float x){
        this.x = x;
    }

    /**
     *
     * @return
     */
    public float getX() {
        return x;
    }
}
