package de.rwth_aachen.sensor;


import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Tab used to represent gyroscope data
 * Created by admin on 12.05.2015.
 */
public class TabGyroscope extends AbstractTab {

    @Override
    public void draw(Dataset data){
        ImageView iv = (ImageView) getView().findViewById(R.id.imageView);
        Bitmap bm = DrawGraph.draw3D(data.getDataGyroscope(), data.getMinDataGyroscope(), data.getMaxDataGyroscope(), iv);
        iv.setImageBitmap(bm);
    }
}
