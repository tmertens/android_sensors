package de.rwth_aachen.sensor;

import java.util.ArrayList;

/**
 * Stores and manages all data of the sensors
 * Created by admin on 13.05.2015.
 */
public class Dataset {

    // Datasets
    private final ArrayList<Point3D> dataAccelerometer;
    private final ArrayList<Point3D> dataMagnetometer;
    private final ArrayList<Point3D> dataGyroscope;
    private final ArrayList<Point1D> dataBarometer;

    // Store min/max values in each dataset
    private float minDataAccelerometer;
    private float maxDataAccelerometer;
    private float minDataMagnetometer;
    private float maxDataMagnetometer;
    private float minDataGyroscope;
    private float maxDataGyroscope;
    private float minDataBarometer;
    private float maxDataBarometer;

    /**
     * Default constructor to initialize lists
     */
    public Dataset(){
        dataAccelerometer = new ArrayList<>();
        dataMagnetometer = new ArrayList<>();
        dataGyroscope = new ArrayList<>();
        dataBarometer = new ArrayList<>();
    }

    /**
     *
     * @return
     */
    public ArrayList<Point3D> getDataAccelerometer() {
        return dataAccelerometer;
    }

    /**
     *
     * @return
     */
    public ArrayList<Point3D> getDataMagnetometer() {
        return dataMagnetometer;
    }

    /**
     *
     * @return
     */
    public ArrayList<Point3D> getDataGyroscope() {
        return dataGyroscope;
    }

    /**
     *
     * @return
     */
    public ArrayList<Point1D> getDataBarometer() {
        return dataBarometer;
    }

    /**
     * Add new point to dataset of the sensor
     * Manages min and max values
     * @param p
     */
    public void addDataAccelerometer(Point3D p){
        // min/max value in dataset
        minDataAccelerometer = Math.min( Math.min(minDataAccelerometer,p.getX()), Math.min(p.getY(),p.getZ()) );
        maxDataAccelerometer = Math.max( Math.max(maxDataAccelerometer,p.getX()), Math.max(p.getY(),p.getZ()) );
        // add point
        dataAccelerometer.add(p);
    }

    /**
     * Add new point to dataset of the sensor
     * Manages min and max values
     * @param p
     */
    public void addDataMagnetometer(Point3D p){
        // min/max value in dataset
        minDataMagnetometer = Math.min( Math.min(minDataMagnetometer,p.getX()), Math.min(p.getY(),p.getZ()) );
        maxDataMagnetometer = Math.max( Math.max(maxDataMagnetometer,p.getX()), Math.max(p.getY(),p.getZ()) );
        // add point
        dataMagnetometer.add(p);
    }

    /**
     * Add new point to dataset of the sensor
     * Manages min and max values
     * @param p
     */
    public void addDataGyroscope(Point3D p){
        // min/max value in dataset
        minDataGyroscope = Math.min( Math.min(minDataGyroscope,p.getX()), Math.min(p.getY(),p.getZ()) );
        maxDataGyroscope = Math.max( Math.max(maxDataGyroscope,p.getX()), Math.max(p.getY(),p.getZ()) );
        // add point
        dataGyroscope.add(p);
    }

    /**
     * Add new point to dataset of the sensor
     * Manages min and max values
     * @param p
     */
    public void addDataBarometer(Point1D p){
        // min/max value in dataset
        minDataBarometer = Math.min(minDataBarometer,p.getX());
        maxDataBarometer = Math.max(maxDataBarometer,p.getX());
        // add point
        dataBarometer.add(p);
    }

    /**
     *
     * @return
     */
    public float getMinDataAccelerometer() {
        return minDataAccelerometer;
    }

    /**
     *
     * @return
     */
    public float getMaxDataAccelerometer() {
        return maxDataAccelerometer;
    }

    /**
     *
     * @return
     */
    public float getMinDataMagnetometer() {
        return minDataMagnetometer;
    }

    /**
     *
     * @return
     */
    public float getMaxDataMagnetometer() {
        return maxDataMagnetometer;
    }

    /**
     *
     * @return
     */
    public float getMinDataGyroscope() {
        return minDataGyroscope;
    }

    /**
     *
     * @return
     */
    public float getMaxDataGyroscope() {
        return maxDataGyroscope;
    }

    /**
     *
     * @return
     */
    public float getMinDataBarometer() {
        return minDataBarometer;
    }

    /**
     *
     * @return
     */
    public float getMaxDataBarometer() {
        return maxDataBarometer;
    }
}
