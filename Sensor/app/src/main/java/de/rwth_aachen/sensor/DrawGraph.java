package de.rwth_aachen.sensor;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by admin on 12.05.2015.
 */
public class DrawGraph {

    // Draw 3D dataset to bitmap
    public static Bitmap draw3D(ArrayList<Point3D> data, float minValue, float maxValue, ImageView iv){

        // Define max plotted values
        int maxXValues = 100;

        // Get dimensions of outer imageview
        int height = iv.getHeight();
        int width = iv.getWidth();

        // Limit maxXValues by width
        if(maxXValues>width){
            maxXValues=width;
        }
        // Calculate step size of x axis
        float stepSizeXAxis = width/maxXValues;

        // Calculate step size of y axis
        float stepSizeYAxis=1;
        if((maxValue-minValue)>0) {
            stepSizeYAxis = height / (maxValue-minValue);
        }
        // Calculate 0 line
        float middle = height + stepSizeYAxis * minValue;

        // Create bitmap and canvas
        Bitmap bm = Bitmap.createBitmap(width,height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bm);

        // Set x curve style
        Paint xCurvePaint = new Paint();
        xCurvePaint.setColor(Color.BLUE);
        xCurvePaint=setLineStyle(xCurvePaint);

        // Set y curve style
        Paint yCurvePaint = new Paint();
        yCurvePaint.setColor(Color.GREEN);
        yCurvePaint=setLineStyle(yCurvePaint);

        // Set z curve style
        Paint zCurvePaint = new Paint();
        zCurvePaint.setColor(Color.RED);
        zCurvePaint=setLineStyle(zCurvePaint);

        // Initialize paths
        Path xCurve = new Path();
        Path yCurve = new Path();
        Path zCurve = new Path();

        // Move start to element with index (dataLength-maxXValues), specify offset
        int start = 0;
        int offset= 0;
        if(data.size()>maxXValues){
            start=data.size()-maxXValues;
            offset=data.size()-maxXValues;
        }

        // Move to first point in data
        if(data.size()>0) {
            xCurve.moveTo(0, middle - stepSizeYAxis * data.get(start).getX());
            yCurve.moveTo(0, middle - stepSizeYAxis * data.get(start).getY());
            zCurve.moveTo(0, middle - stepSizeYAxis * data.get(start).getZ());
        }

        // Draw further points
        for(int i=start+1;i<data.size();i++){
            Point3D p = data.get(i);
            xCurve.lineTo((float) stepSizeXAxis * (i-offset), middle - (float) stepSizeYAxis * p.getX());
            yCurve.lineTo((float) stepSizeXAxis * (i-offset), middle - (float) stepSizeYAxis * p.getY());
            zCurve.lineTo((float) stepSizeXAxis * (i-offset), middle - (float) stepSizeYAxis * p.getZ());
        }

        canvas.drawPath(xCurve, xCurvePaint);
        canvas.drawPath(yCurve, yCurvePaint);
        canvas.drawPath(zCurve, zCurvePaint);

        return bm;
    }

    // Draw 1D dataset to bitmap
    public static Bitmap draw1D(ArrayList<Point1D> data, float minValue, float maxValue, ImageView iv){

        // Define max plotted values
        int maxXValues = 100;

        // Get dimensions of outer imageview
        int height = iv.getHeight();
        int width = iv.getWidth();

        // Limit maxXValues by width
        if(maxXValues>width){
            maxXValues=width;
        }
        // Calculate step size of x axis
        float stepSizeXAxis = width/maxXValues;

        // Calculate step size of y axis
        float stepSizeYAxis=1;
        if((maxValue-minValue)>0) {
            stepSizeYAxis = height / (maxValue-minValue);
        }
        // Calculate 0 line
        float middle = height + stepSizeYAxis * minValue;

        // Create bitmap and canvas
        Bitmap bm = Bitmap.createBitmap(width,height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bm);

        // Set x curve style
        Paint xCurvePaint = new Paint();
        xCurvePaint.setColor(Color.BLUE);
        xCurvePaint=setLineStyle(xCurvePaint);

        // Initialize paths
        Path xCurve = new Path();

        // Move start to element with index (dataLength-maxXValues), specify offset
        int start = 0;
        int offset= 0;
        if(data.size()>maxXValues){
            start=data.size()-maxXValues;
            offset=data.size()-maxXValues;
        }

        // Move to first point in data
        if(data.size()>0) {
            xCurve.moveTo(0, middle - stepSizeYAxis * data.get(start).getX());
        }

        // Draw further points
        for(int i=start+1;i<data.size();i++){
            Point1D p = data.get(i);
            xCurve.lineTo((float) stepSizeXAxis * (i-offset), middle - (float) stepSizeYAxis * p.getX());
        }

        canvas.drawPath(xCurve, xCurvePaint);

        return bm;
    }

    // Define general line style
    private static Paint setLineStyle(Paint p){
        p.setStrokeWidth(2f);
        p.setStyle(Paint.Style.STROKE);
        p.setStrokeJoin(Paint.Join.ROUND);
        p.setStrokeCap(Paint.Cap.ROUND);
        p.setPathEffect(new CornerPathEffect(10));
        p.setAntiAlias(true);
        return p;
    }

}
