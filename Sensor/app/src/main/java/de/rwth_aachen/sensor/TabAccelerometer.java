package de.rwth_aachen.sensor;


import android.app.ActionBar;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Tab used to represent accelerometer data
 * Created by admin on 12.05.2015.
 */
public class TabAccelerometer extends AbstractTab {

    @Override
    public void draw(Dataset data){
        ImageView iv = (ImageView) getView().findViewById(R.id.imageView);
        Bitmap bm = DrawGraph.draw3D(data.getDataAccelerometer(), data.getMinDataAccelerometer(), data.getMaxDataAccelerometer(), iv);
        iv.setImageBitmap(bm);
    }
}
