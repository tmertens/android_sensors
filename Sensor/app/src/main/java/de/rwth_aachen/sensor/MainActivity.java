package de.rwth_aachen.sensor;


import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Main Class
 */
public class MainActivity extends FragmentActivity implements SensorEventListener, ActionBar.TabListener {

    // Global variables
    private SensorManager senSensorManager;
    private Sensor senAccelerometer,senMagnetic, senGyro, senBaro;
    private UpdateTimes updateTimes;
    private Point3D lastPoint;
    private UDPClient Client;
    private Boolean client_generated =false;
    private Dataset data;

    // GUI variables
    private ViewPager viewPager;
    private TabsPagerAdapter tabsPagerAdapter;

    private static Handler handler = new Handler();
    private final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Client.generateAndSendMessage(3);
            /* and here comes the "trick" */
            handler.postDelayed(this, 15000);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialization of Sensors
        updateTimes = new UpdateTimes();
        lastPoint = new Point3D(0,0,0);
        data = new Dataset();
        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senMagnetic  = senSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        senGyro = senSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        senBaro = senSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        senSensorManager.registerListener(this, senMagnetic, SensorManager.SENSOR_DELAY_NORMAL);
        senSensorManager.registerListener(this, senGyro, SensorManager.SENSOR_DELAY_NORMAL);
        senSensorManager.registerListener(this, senBaro, SensorManager.SENSOR_DELAY_NORMAL);

        // Initialization of Tabs
        viewPager = (ViewPager) findViewById(R.id.pager);
        tabsPagerAdapter = new TabsPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(tabsPagerAdapter);
        // Keep all sites in memory
        viewPager.setOffscreenPageLimit(3);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        long curTime = System.currentTimeMillis();
        Sensor mySensor = sensorEvent.sensor;
        AbstractTab tab = null;

        if(mySensor.getType() == Sensor.TYPE_PRESSURE && (curTime - updateTimes.getLastUpdateBaro()) > 200){
            tab = tabsPagerAdapter.getTab(3);
            Point1D point = new Point1D(sensorEvent.values[0]);
            data.addDataBarometer(point);

            String text = "AMBIENT PRESSURE: \t <font color='blue'>" + point.getX() + "</font>hPa";
            if (tab!=null) tab.setText(text);
            updateTimes.setLastUpdateBaro(curTime);
        }

        if(mySensor.getType() == Sensor.TYPE_GYROSCOPE && (curTime - updateTimes.getLastUpdateGyro()) > 200){
            tab = tabsPagerAdapter.getTab(0);
            Point3D point = new Point3D(sensorEvent.values[0],sensorEvent.values[1],sensorEvent.values[2]);
            data.addDataGyroscope(point);

            String text = "GYROSCOPE: \t x: <font color='blue'>" + point.getX() + "</font>\t/y: <font color='green'>" + point.getY() + "</font>\t/z: <font color='red'>" + point.getZ() + "</font>";
            if(tab!=null)tab.setText(text);
            updateTimes.setLastUpdateGyro(curTime);
        }

        if (mySensor.getType() == Sensor.TYPE_MAGNETIC_FIELD && (curTime - updateTimes.getLastUpdateMag()) > 200){
            tab = tabsPagerAdapter.getTab(1);
            Point3D point = new Point3D(sensorEvent.values[0],sensorEvent.values[1],sensorEvent.values[2]);
            data.addDataMagnetometer(point);

            String text = "MAGNETIC: \t x: <font color='blue'>" + point.getX() + "</font>\t/y: <font color='green'>" + point.getY() + "</font>\t/z: <font color='red'>" + point.getZ() + "</font>";
            if(tab!=null)tab.setText(text);
            updateTimes.setLastUpdateMag(curTime);
        }

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER && (curTime - updateTimes.getLastUpdateAcc()) > 200) {
            tab = tabsPagerAdapter.getTab(2);
            Point3D point = new Point3D(sensorEvent.values[0],sensorEvent.values[1],sensorEvent.values[2]);
            data.addDataAccelerometer(point);

            // Detect device shake
            long diffTime = (curTime - updateTimes.getLastUpdateAcc());
            float speed = Math.abs(point.getX() + point.getY() + point.getZ() - lastPoint.getX() - lastPoint.getY() - lastPoint.getZ())/ diffTime * 10000;
            if (speed > 750) {
                Toast.makeText(this, "shake detected w/ speed: " + speed, Toast.LENGTH_SHORT).show();
                if((client_generated)&&(Client.ds!=null)) {
                    Client.generateAndSendMessage(4);
                }
            }
            // save lastPoint for shake detection
            lastPoint = new Point3D(point);

            String text = "ACCELEROMETER: \t x: <font color='blue'>" + point.getX() + "</font>\t/y: <font color='green'>"+ point.getY()  + "</font>\t/z: <font color='red'>" + point.getZ() + "</font>";
            if(tab!=null)tab.setText(text);
            updateTimes.setLastUpdateAcc(curTime);
        }

        // (Re-)Draw current graph
        if((curTime - updateTimes.getLastDrawn()) > 200){
            if(tab!=null && viewPager.getCurrentItem()==tab.getIndex()){
                tab.draw(data);
                updateTimes.setLastDrawn(curTime);
            }
        }
    }

    /**
     * Handles onclick events of connect-/ disconnect button
     * @param view view element of the event
     */
    public void onClickButton (View view) {
        Button disconnect = (Button) this.findViewById(R.id.button2);
        Button connect = (Button) this.findViewById(R.id.button);
        switch (view.getId()) {
            case R.id.button:
                // Connect button
                EditText host_edit = (EditText) this.findViewById(R.id.host_e);
                EditText port_edit = (EditText) this.findViewById(R.id.port_e);

                String host= host_edit.getText().toString();
                int port = Integer.parseInt(port_edit.getText().toString());
                Client = new UDPClient(this,host,port);
                //Send message
                Client.generateAndSendMessage(1);
                Client.newThread();
                handler  =  new Handler();
                handler.postDelayed(runnable, 0); //Responsible for KEEPALIVE
                client_generated=true;

                disconnect.setEnabled(true);
                connect.setEnabled(false);
            break;
            case R.id.button2:
                // Disconnect button
                Client.generateAndSendMessage(2);
                handler.removeCallbacks(runnable);
                disconnect.setEnabled(false);
                connect.setEnabled(true);
            break;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onPause(){
        super.onPause();
        senSensorManager.unregisterListener(this);
        handler.removeCallbacks(runnable);

    }

    @Override
    protected void onResume() {
        Button disconnect = (Button) this.findViewById(R.id.button2);
        Button connect = (Button) this.findViewById(R.id.button);
        super.onResume();
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        senSensorManager.registerListener(this, senMagnetic, SensorManager.SENSOR_DELAY_NORMAL);
        senSensorManager.registerListener(this, senGyro, SensorManager.SENSOR_DELAY_NORMAL);
        senSensorManager.registerListener(this, senBaro, SensorManager.SENSOR_DELAY_NORMAL);
        disconnect.setEnabled(false);
        connect.setEnabled(true);
    }


    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        // on tab selected show respected fragment view
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

}
