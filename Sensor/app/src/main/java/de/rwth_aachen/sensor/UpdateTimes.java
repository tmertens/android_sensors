package de.rwth_aachen.sensor;

/**
 * Class managing all updateTimes of the sensors and graphs
 * Created by admin on 12.05.2015.
 */
class UpdateTimes {

    private long lastUpdateGyro;
    private long lastUpdateMag;
    private long lastUpdateAcc;
    private long lastUpdateBaro;
    private long lastDrawn;

    /**
     *
     * @return
     */
    public long getLastUpdateGyro() {
        return lastUpdateGyro;
    }

    /**
     *
     * @param lastUpdateGyro
     */
    public void setLastUpdateGyro(long lastUpdateGyro) {
        this.lastUpdateGyro = lastUpdateGyro;
    }

    /**
     *
     * @return
     */
    public long getLastUpdateMag() {
        return lastUpdateMag;
    }

    /**
     *
     * @param lastUpdateMag
     */
    public void setLastUpdateMag(long lastUpdateMag) {
        this.lastUpdateMag = lastUpdateMag;
    }

    /**
     *
     * @return
     */
    public long getLastUpdateAcc() {
        return lastUpdateAcc;
    }

    /**
     *
     * @param lastUpdateAcc
     */
    public void setLastUpdateAcc(long lastUpdateAcc) {
        this.lastUpdateAcc = lastUpdateAcc;
    }

    /**
     *
     * @return
     */
    public long getLastUpdateBaro() {
        return lastUpdateBaro;
    }

    /**
     *
     * @param lastUpdateBaro
     */
    public void setLastUpdateBaro(long lastUpdateBaro) {
        this.lastUpdateBaro = lastUpdateBaro;
    }

    /**
     *
     * @return
     */
    public long getLastDrawn() {
        return lastDrawn;
    }

    /**
     *
     * @param lastDrawn
     */
    public void setLastDrawn(long lastDrawn) {
        this.lastDrawn = lastDrawn;
    }
}
