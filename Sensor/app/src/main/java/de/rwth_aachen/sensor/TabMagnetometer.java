package de.rwth_aachen.sensor;


import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Tab used to represent magnetometer data
 * Created by admin on 12.05.2015.
 */
public class TabMagnetometer extends AbstractTab {

    @Override
    public void draw(Dataset data){
        ImageView iv = (ImageView) getView().findViewById(R.id.imageView);
        Bitmap bm = DrawGraph.draw3D(data.getDataMagnetometer(), data.getMinDataMagnetometer(), data.getMaxDataMagnetometer(), iv);
        iv.setImageBitmap(bm);
    }
}
