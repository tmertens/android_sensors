package de.rwth_aachen.sensor;

/**
 * Class representing a point with three coordinates (3D)
 * Created by admin on 12.05.2015.
 */
class Point3D {

    private final float x;
    private final float y;
    private final float z;

    /**
     * Default constructor
     * @param x x-coordinate
     * @param y y-coordinate
     * @param z z-coordinate
     */
    public Point3D(float x, float y, float z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Copy constructor
     * @param p point to be copied
     */
    public Point3D(Point3D p){
        this.x = p.x;
        this.y = p.y;
        this.z = p.z;
    }

    /**
     *
     * @return
     */
    public float getX() {
        return x;
    }

    /**
     *
     * @return
     */
    public float getY() {
        return y;
    }

    /**
     *
     * @return
     */
    public float getZ() {
        return z;
    }

}
