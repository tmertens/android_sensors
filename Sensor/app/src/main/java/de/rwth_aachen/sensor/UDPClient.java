package de.rwth_aachen.sensor;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.sql.Timestamp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.Toast;

/**
 * Provides functionality to send and receive required UDP packets
 * Created by Thomas on 09.05.2015.
 */
class UDPClient {
    private final String host;
    private final int port;
    private final Activity activity;
    private AsyncTask<Void, Void, Void> async_client;

    DatagramSocket ds = null;
    private final byte[] res = new byte[1024];

    /**
     * Default constructor
     * Creates new UDP socket
     * @param activity  reference to activity
     * @param host server
     * @param port serverport
     */
    public UDPClient(Activity activity, String host, int port) {
        this.activity = activity;
        this.host = host;
        this.port = port;
        try {
            ds = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    /**
     * Generates the message according to the defined format and calls the function to send
     * @param _type message type
     */
    public void generateAndSendMessage(int _type){
        String username = android.os.Build.MODEL;
        byte[] result;
        if(_type == 4) { //EVENT
            double NTP_SCALE = 4294967296.0;
            long unixTime = System.currentTimeMillis()/1000L;
            result =  new byte[9];
            result[0]= (byte) _type;
            int k = 0;
            double d = unixTime / NTP_SCALE;
            for (int i = 0; i < 8; ++i) {
                if ((k = (int) (d *= 256.0)) >= 256) {
                    k = 255;
                }
                result[i+1] = (byte) k;
                d -= k;
            }
        }else if(_type==1){
            byte [] result2 = username.getBytes();
            result = new byte[result2.length+2];
            result[0]=(byte) _type;
            result[1]=(byte) result2.length;
            for(int i=0;i<result2.length;i++){
                result[i+2]=result2[i];
            }
        }else {
            result = new byte[]{(byte) _type};
        }
        SendMessage(result);
    }

    /**
     * Creates thread to wait for incoming messages
     * Displays received message
     */
    public void newThread(){
        new Thread(){
            public Timestamp zeit_val;

            public void run() {

                while(true){
                    DatagramPacket p = new DatagramPacket(res, res.length);
                    try {
                        ds.receive(p);
                        if(p.getLength()>9) {
                            if(p.getLength()>=(9+ (int) res[9])) {
                                byte[] zeit = new byte[8];
                                for (int i = 1; i < 9; i++) {
                                    zeit[i - 1] = res[i];
                                }
                                int zeit_value = ByteBuffer.wrap(zeit).getInt();
                                System.out.println(zeit_value);
                                final String name = new String(res, 10, res[9]);
                                zeit_val =  new Timestamp(zeit_value * 1000L);
                                activity.runOnUiThread(new Runnable()
                                {
                                    public void run()
                                    {
                                        Toast.makeText(activity,"Received shake event of " + name + " at: " + zeit_val.toString(), Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }}}.start();
    }

    @SuppressLint("NewApi")
    /**
     * Creates AsyncTask to send packet via UDP
     * @param message message
     */
    private void SendMessage(final byte[] message) {
        async_client = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                DatagramPacket dp;
                try {
                    dp = new DatagramPacket(message, message.length, InetAddress.getByName(host), port);
                    ds.setBroadcast(true);
                    ds.send(dp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
            }
        };

        if (Build.VERSION.SDK_INT >= 11)
            async_client.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else async_client.execute();
    }
}
