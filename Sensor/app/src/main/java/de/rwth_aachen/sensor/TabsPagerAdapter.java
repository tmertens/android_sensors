package de.rwth_aachen.sensor;


import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.HashMap;

/**
 * Created by admin on 12.05.2015.
 */
class TabsPagerAdapter extends FragmentPagerAdapter{

    // Number of tabs
    private static final int NUM_TABS=4;
    // Tab titles
    private final String[] tabTitles = { "Gyroscope", "Magnetometer", "Accelerometer", "Barometer" };

    private final HashMap<Integer, AbstractTab> tabs;

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
        tabs = new HashMap<>();
    }

    @Override
    public AbstractTab getItem(int index) {

        AbstractTab tab = null;
        switch(index){
            case 0: tab = new TabGyroscope(); break;
            case 1: tab = new TabMagnetometer(); break;
            case 2: tab = new TabAccelerometer(); break;
            case 3: tab = new TabBarometer(); break;
        }

        // Pass index to tab
        Bundle args = new Bundle();
        args.putInt("index", index);
        tab.setArguments(args);

        // Add new tab to hashset
        tabs.put(index, tab);

        return tab;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return NUM_TABS;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    public AbstractTab getTab(int position) {
        if(position<0 || position>=NUM_TABS){
            return null;
        }
        return tabs.get(position);
    }

}
