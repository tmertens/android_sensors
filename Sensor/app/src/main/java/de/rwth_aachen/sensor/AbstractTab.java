package de.rwth_aachen.sensor;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Abstract class for all tabs (fragments)
 * Implements default behaviour
 * Created by admin on 15.05.2015.
 */
public abstract class AbstractTab extends Fragment {

    // Store the unique index of the tab (required for TabsPagerAdapter)
    private int index;

    // Define default tab layout
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.index = getArguments().getInt("index", 0);
        return inflater.inflate(R.layout.tab_default, container, false);
    }

    /**
     * Display sensor data on tab
     * @param text Displayed text
     */
    public void setText(String text){
        TextView tv = (TextView) getView().findViewById(R.id.text);
        tv.setText(Html.fromHtml(text));
    }

    /**
     * Draw graph based on sensor data
     * @param data Sensor data for graph representation
     */
    public abstract void draw(Dataset data);

    /**
     *
     * @return
     */
    public int getIndex() {
        return index;
    }
}
