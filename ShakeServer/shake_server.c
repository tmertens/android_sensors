/* 
 * File:   shake_server.c
 * Author: hellmanns
 *
 * Created on May 16, 2015, 3:36 PM
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <netdb.h>
#include <pthread.h>
#include <time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <inttypes.h>


#define MIN(x, y) (((x) < (y)) ? (x) : (y))

#define MAXEVENTS 20

#define LOCAL_SERVER_PORT 1234
#define BUF 255
#define TYPE_REGISTER 1
#define TYPE_UNREGISTER 2
#define TYPE_KEEPALIVE 3
#define TYPE_EVENT 4
#define TYPE_SHAKE 5

#define SOCK_ADDR_IN_ADDR(sa)	SOCK_ADDR_IN_PTR(sa)->sin_addr
#define SOCK_ADDR_IN_PTR(sa)	((struct sockaddr_in *)(sa))

#define SIZEOF_REGISTER_PACKET 2+length

#define BUFFER_LENGTH 300

typedef struct {
    uint8_t length;
    char *name;
} packet_register;

typedef struct {
    uint8_t type;
} __attribute__ ((__packed__)) packet_keepalive;

typedef struct {
    uint8_t type;
    uint64_t time;
} __attribute__ ((__packed__)) packet_event;

typedef struct {
    uint8_t type;
    uint64_t time;
    uint8_t length;
} __attribute__ ((__packed__)) packet_shake;

typedef struct
{
    int epollfd;
    int srvfd;
    int *active;
} thread_information;

typedef struct
{
    uint8_t lengthName;
    char *name;
    struct sockaddr *addr;
    uint64_t lastKeepAlive;
} client_information;

typedef struct
{
    struct client_info_list *next;
    client_information *clientInfo;
} client_info_list;

typedef struct
{
    uint8_t *answerBuffer;
    uint16_t bytesToSend;
    client_info_list *curReceiver;
    int events;
    int sockfd;
    struct epoll_event *event_struct;
} state_information;


/**
 * checks first and second argument of type struct sockaddr* for equality
 * @param sa pointer to first sockaddr struct
 * @param sb pointer to second sockaddr struct
 * @return if both these equal returns 0 else returns sa-sb
 */
int sockaddrCmpAddr(const struct sockaddr * sa, const struct sockaddr * sb)
{
    if (sa->sa_family != sb->sa_family)
    {
	return (sa->sa_family - sb->sa_family);
    }
    if (sa->sa_family == AF_INET) 
    {
	return (SOCK_ADDR_IN_ADDR(sa).s_addr - SOCK_ADDR_IN_ADDR(sb).s_addr);
    #ifdef HAS_IPV6
    } 
    else if (sa->sa_family == AF_INET6) 
    {
	return (memcmp((char *) &(SOCK_ADDR_IN6_ADDR(sa)),
		       (char *) &(SOCK_ADDR_IN6_ADDR(sb)),
		       sizeof(SOCK_ADDR_IN6_ADDR(sa))));
    #endif
    }
    else 
    {
	fprintf(stderr, "sock_addr_cmp_addr: unsupported address family %d", sa->sa_family);
    }
    
    return -1;
}

/**
 * helper method for enabling/disabling events
 * @param epollfd file descriptor of the associated epoll
 * @param *info pointer to the state_information struct of the socket file descriptor you are interested in
 * @return if success returns 0 else -1
 */
int setInterestOps(int epollfd, state_information *info)
{
    struct epoll_event event;
    event.events = info->events;
    event.data.fd = info->sockfd;
    event.data.ptr = info;
    
    //if socket was not added yet, add it!
    if(epoll_ctl(epollfd, EPOLL_CTL_ADD, info->sockfd, &event) == -1)
    {
        //socket is already added or another error occurred
        //if socket was already modify the interest set
        if(errno == 17 && epoll_ctl(epollfd, EPOLL_CTL_MOD, info->sockfd, &event) != -1)
        {
            return 0;
        }
        
        //another error occured
        fprintf(stderr, "Epoll_ctl failed: %i\n", errno);
        close(info->sockfd);
        return(EXIT_FAILURE);
    }
    
    return 0;
}

/**
 * convenience method for enabling events
 * @param epollfd file descriptor of the associated epoll
 * @param *info pointer to the state_information struct of the socket file descriptor you are interested in
 * @return if success returns 0 else -1
 */
int registerRead(int epollfd, state_information *info)
{
    info->events |= EPOLLIN;
    return setInterestOps(epollfd, info);
}

/**
 * convenience method for disabling reading
 * @param epollfd file descriptor of the associated epoll
 * @param *info pointer to the state_information struct of the socket file descriptor you are interested in
 * @return if success returns 0 else -1
 */
int unregisterRead(int epollfd, state_information *info)
{
    info->events &= ~EPOLLIN;
    return setInterestOps(epollfd, info);
}

/**
 * convenience method for enabling writing
 * @param epollfd file descriptor of the associated epoll
 * @param *info pointer to the state_information struct of the socket file descriptor you are interested in
 * @return if success returns 0 else -1
 */
int registerWrite(int epollfd, state_information *info)
{
    info->events |= EPOLLOUT;
    return setInterestOps(epollfd, info);
}

/**
 * convenience method for disabling writing
 * @param epollfd file descriptor of the associated epoll
 * @param *info pointer to the state_information struct of the socket file descriptor you are interested in
 * @return if success returns 0 else -1
 */
int unregisterWrite(int epollfd, state_information *info)
{
    info->events &= ~EPOLLOUT;
    return setInterestOps(epollfd, info);
}

/**
 * convenience method for setting the nonblocking flag of a file descriptor
 * @param sockfd file descriptor of the socket which should set nonblocking
 * @return if success returns 0 else -1
 */
int setSocketNonBlocking(int sockfd)
{
    //get all flags
    int flags = fcntl(sockfd, F_GETFL, 0);
    
    //did an error occurr?
    if(flags == -1)
    {
        fprintf(stderr, "Getting flags of socket failed: %i\n", errno);
        return(EXIT_FAILURE);
    }
    
    //add the nonblock flag
    flags |= O_NONBLOCK;
    
    //write flags
    if(fcntl(sockfd, F_SETFL, flags))
    {
        fprintf(stderr, "Setting flags of socket failed: %i\n", errno);
        return(EXIT_FAILURE);
    }
    
    return(EXIT_SUCCESS);
}

/**
 * register a device as a broadcast listener and potential sender
 * @param deviceList linked list of the current registered clients
 * @param clientInfo struct containing the information about the new client
 * @return if success returns 0 else -1
 */
int registerDevice(client_info_list **deviceList, client_information *clientInfo)
{
    //allocate memory for new list entry
    client_info_list *newEntry = malloc(sizeof(client_info_list));
    if(newEntry == NULL)
    {
       perror("Could not allocate memory for new list entry");
       return EXIT_FAILURE;
    }
    
    //store pointer to the clientInfo of the new device
    newEntry->clientInfo = clientInfo;

    //is list empty?    
    if(*deviceList != NULL)
    {
       //copy pointer to the first element
       client_info_list *curEntry = (client_info_list*)*deviceList;

       //iterate list to check if device is already registered
       while(curEntry != NULL)
       {
          //check name of new and current device to find duplicates
          if(strcmp(clientInfo->name, curEntry->clientInfo->name) == 0)
          {
             return EXIT_FAILURE;
          }
          //next element
          curEntry = (client_info_list*)curEntry->next;
       }
       
       //device was not already registered -> add front
       newEntry->next = (struct client_info_list*)*deviceList;
       *deviceList = newEntry;
    }
    else
    {
        //list is empty, add first element
       *deviceList = newEntry;
    }

    return EXIT_SUCCESS;
}

/**
 * unregister a device as a broadcast listener and potential sender
 * @param deviceList linked list of the current registered clients
 * @param clientInfo struct containing the information about the new client
 * @return if success returns 0 else -1
 */
int unregisterDevice(client_info_list **deviceList, struct sockaddr *sender)
{
    //create pointer for iterating
    client_info_list *curEntry = (client_info_list*)*deviceList;
    //create pointer to the previous element of curEntry
    client_info_list *prevEntry = NULL;
    
    //iterate list to find the element which should be deleted
    while(curEntry != NULL)
    {
       if(sockaddrCmpAddr(curEntry->clientInfo->addr, sender) == 0)
       {
           //is it the first element
           if(prevEntry != NULL)
           {
               //update next pointer to delete element from list
               prevEntry->next = curEntry->next;
           }
           else
           {
               //update head pointer of the list
               *deviceList = (client_info_list*)curEntry->next;
           }
           
           //free the memory of the device entry
           free(curEntry->clientInfo->name);
           free(curEntry->clientInfo->addr);
           free(curEntry->clientInfo);
           free(curEntry);
           return EXIT_SUCCESS;
       }
       //iterate
       prevEntry = curEntry;
       curEntry = (client_info_list*)curEntry->next;
    }
    return EXIT_FAILURE;
}

/**
 * is invoked, in case a shake packet arrived to create the broadcast packet
 * @param deviceList linked list of the current registered clients
 * @param sender sockaddr struct describing the sender 
 * @param event the event packet arrived from the sender
 * @param state_info a pointer to the struct which holds the global state of our server socket
 * @return if success returns 0 else -1
 */
int broadcastShake(client_info_list **deviceList, struct sockaddr *sender, packet_event *event, state_information *state_info)
{
    client_info_list *curEntry = *deviceList;
    //client_info_list *searchElement = *deviceList;
    packet_shake *shakePacket = NULL;

    while(curEntry != NULL)
    {
       //check if the sender of the event is registered
       if(sockaddrCmpAddr(curEntry->clientInfo->addr, sender) == 0)
       {
           //allocate memory for the shake packet
           shakePacket = malloc(sizeof(packet_shake)+curEntry->clientInfo->lengthName);
           if(shakePacket == NULL)
           {
               perror("Could not allocate memory for shake broadcast");
               return EXIT_FAILURE;
           }
           //initialize shake packet
           shakePacket->type = TYPE_SHAKE;
           shakePacket->time = event->time;
           shakePacket->length = curEntry->clientInfo->lengthName;
           memcpy(((uint8_t*)shakePacket)+sizeof(packet_shake), curEntry->clientInfo->name, curEntry->clientInfo->lengthName); 
           
           //set state information to enable the broadcast
           state_info->answerBuffer = (uint8_t*)shakePacket;
           state_info->bytesToSend = sizeof(packet_shake)+curEntry->clientInfo->lengthName;
           state_info->curReceiver = *deviceList;
       }
       //iterate
       curEntry = (client_info_list*)curEntry->next;
    }
    return EXIT_SUCCESS;
}

/**
 * is invoked, in case a keepalive arrives and the timeout of the sender has to be updated
 * @param deviceList linked list of the current registered clients
 * @param sender the sockaddr struct describing the sender
 * @return if success returns 0 else -1
 */
int keepAliveReceived(client_info_list **deviceList, struct sockaddr *sender)
{
    client_info_list *curEntry = (client_info_list*)*deviceList;
    
    //iterate list
    while(curEntry != NULL)
    {
       //search entry of the device
       if(sockaddrCmpAddr(curEntry->clientInfo->addr, sender) == 0)
       {
           //update keep alive
           curEntry->clientInfo->lastKeepAlive = time(NULL);
           return EXIT_SUCCESS;
       }
       //iterate
       curEntry = (client_info_list*)curEntry->next;
    }
    return EXIT_FAILURE;
}

/**
 * is invoked, to handle incoming packets
 * @param epollfd file descriptor of the triggering epoll
 * @param buffer read buffer
 * @param deviceList linked list of the current registered clients
 * @return if success returns 0 else -1
 */
void readCallback(int epollfd, state_information *state_info, uint8_t *buffer, client_info_list **deviceList)
{
    int32_t readableBytes = 0;
    int sockfd = state_info->sockfd;
    
    //Allocate memory for ClientAddr    
    struct sockaddr *clientAddr = malloc(sizeof(struct sockaddr));
    socklen_t lenSockAddr = sizeof(*clientAddr);
    
    //Put receive into buffer
    readableBytes = recvfrom(sockfd, buffer, BUFFER_LENGTH, 0, clientAddr, &lenSockAddr);
    
    if(readableBytes > 0)
    {
        packet_keepalive *keepalive = (packet_keepalive *) buffer;
        
        uint8_t packetType = keepalive->type;
        //Switch on packetType        
        switch(packetType)
        {
            //Register message
            case 1:
                if(readableBytes > 1)
                {
                    uint8_t length = buffer[1];
                    
                    //Create Clientinfo and fill it with data
                    client_information *clientInfo = malloc(sizeof(client_information));
                    if(clientInfo == NULL)
                    {
                        perror("Could not allocate memory for client");
                        return;
                    }

                    clientInfo->name = malloc(sizeof(char)*length);
                    if(clientInfo->name == NULL)
                    {
                       perror("Could not allocate memory for client name");
                       return;
                    }

                    clientInfo->addr = clientAddr;
                    clientInfo->lengthName = length;
                    clientInfo->lastKeepAlive = (unsigned)time(NULL);
                    memcpy(clientInfo->name, (char*)buffer+2, length);

                    //Put in device list
                    if(registerDevice(deviceList, clientInfo) != EXIT_SUCCESS)
                    {
                        fprintf(stderr, "Register failed!\n");
                    }
                    else
                    {
                        printf("Register successful for %s !\n", clientInfo->name);
                    }                                     
                }
                else
                {
                    perror("Incorrect package of type 1");
                    exit(EXIT_FAILURE);
                }
                break;
            case 2:
            //unregister message
                if(readableBytes == 1)
                {
                    //remove device from list
                    if(unregisterDevice(deviceList, clientAddr) != EXIT_SUCCESS)
                    {
                        fprintf(stderr, "Unregister failed!\n");
                    }
                    else
                    {
                        printf("Unregister successful!\n");
                    }
                }
                else
                {
                    perror("Incorrect package of type 2");
                    exit(EXIT_FAILURE);
                }
                break;
            case 3:
            //keepalive
                if(readableBytes == 1)
                {
                    //Update information in device list
                    if(keepAliveReceived(deviceList, clientAddr) != EXIT_SUCCESS)
                    {
                        fprintf(stderr, "Keepalive failed!\n");
                    }
                    else
                    {
                        printf("Keepalive successful!\n");
                    }
                }
                else
                {
                    perror("Incorrect package of type 3");
                    exit(EXIT_FAILURE);
                }
                break;
            case 4:
            //Shake message
                if(readableBytes == 9)
                {
                    packet_event *event = (packet_event*) buffer;
                    int knownClient = -1;
                    client_info_list *curEntry = *deviceList;
                    
                    //Check if client is in device list
                    while (curEntry != NULL){
                        if(knownClient!=0)
                        {
                                knownClient = sockaddrCmpAddr(curEntry->clientInfo->addr , clientAddr);
                        }
                        curEntry =  (client_info_list*) curEntry->next;
                    }
                    
                    if(knownClient==0)
                    {                    
                        //Generate message if client is in device list
                        if(broadcastShake(deviceList, clientAddr, event, state_info) == EXIT_SUCCESS)
                        {
                            state_info->curReceiver = *deviceList;
                            registerWrite(epollfd, state_info);
                            unregisterRead(epollfd, state_info);
                            printf("Successfully created shake_packet\n");
                        }
                        else
                        {
                            fprintf(stderr, "Create shake_packet failed\n");
                        }
                    }
                    else
                    {
                        fprintf(stderr, "Registered a shake event of a not registered device!\n");   
                    }
                }
                else
                {
                    perror("Incorrect package of type 4");
                    exit(EXIT_FAILURE);
                }
                break;
        }
        printf("Type: %i \n", packetType);
    }
    else
    {
        perror("recvfrom");
    }
}


/**
 * is invoked, to handle write operations (broadcasting a shake)
 * @param epollfd file descriptor of the triggering epoll
 * @param state_info pointer to the struct holding the global state of the server socket
 * @return if success returns 0 else -1
 */
void writeCallback(int epollfd, state_information *state_info)
{
    //check for remaining receivers 
    if(state_info->curReceiver != NULL)
    {
        //filter out sender of shake
        if(strcmp(state_info->curReceiver->clientInfo->name, (char *)state_info->answerBuffer+sizeof(packet_shake)) != 0) 
        {
           //send shake packet
           if(sendto(state_info->sockfd, state_info->answerBuffer, state_info->bytesToSend, 0 , state_info->curReceiver->clientInfo->addr, sizeof(struct sockaddr)))
           {
                   perror("ERROR sending message");
                   exit(1);
           }
           else
           {
               printf("Message sent");
           }
        }
        
       //mark next receiver of shake packet
       state_info->curReceiver = (client_info_list*)state_info->curReceiver->next;
    }

    //are all receivers informed?
    if(state_info->curReceiver == NULL)
    {
        //restore state
        unregisterWrite(epollfd, state_info);
        registerRead(epollfd, state_info);
	//tidy up
        free(state_info->answerBuffer);
    }
}

/**
 * convenience method to create a server socket
 * @param port the port the new socket should be bound to
 * @param epollfd the file descriptor of the responsible epoll
 * @return if success returns 0 else -1
 */
int createSrvSocket(char *port, int epollfd)
{
    struct addrinfo hints, *serverInfo, *curAddrInfo;
    int status;
    int srvSocket = -1;
        
    //overwrite former content of hints
    memset(&hints, 0, sizeof hints);
    
    //set conditions for the getaddrinfo call
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE;
        
    //get all matching addrinfo structs
    if((status = getaddrinfo(NULL, port, &hints, &serverInfo)) != 0)
    {
        fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
        exit(EXIT_FAILURE);
    }
    
    //iterate over the set of addrinfo structs until a socket is successfully created
    for(curAddrInfo = serverInfo; curAddrInfo != NULL; curAddrInfo = curAddrInfo->ai_next)
    {
        srvSocket = socket(curAddrInfo->ai_family, curAddrInfo->ai_socktype, curAddrInfo->ai_protocol);
        
        if(srvSocket == -1)
            continue;
        
        break;
    }
    
    //if no suitable struct is found, the socket cannot be created -> return
    if(srvSocket == -1)
    {
        fprintf(stderr, "No suitable addrinfo found: %i\n", errno);
        return(EXIT_FAILURE);
    }
    
    //set the nonblocking flag of the socket
    if(setSocketNonBlocking(srvSocket) == -1)
    {
        fprintf(stderr, "SetSocketNonBlocking failed: %i\n", errno);
        return(EXIT_FAILURE);
    }
   
    //bind socket to the above identified address
    if(bind(srvSocket, curAddrInfo->ai_addr, curAddrInfo->ai_addrlen) == -1)
    {
        fprintf(stderr, "Bind failed: %i\n", errno);
        return(EXIT_FAILURE);
    }
        
    //set persistent information for the server socket
    state_information *newInfo = malloc(sizeof(state_information));
    newInfo->events = EPOLLIN | EPOLLET | EPOLLONESHOT;
    newInfo->sockfd = srvSocket;
    
    //activate above defined events for the server socket
    setInterestOps(epollfd, newInfo);
    
    return srvSocket;
}

int main (int argc, char *argv[])
{
    int i;
    int epollfd;
    int srvfd;
    int active = 1;
    
    int event_counter;
    
    struct epoll_event events[MAXEVENTS];
    
    uint8_t *readBuffer;
    
    uint64_t lastHouseKeeping = 0;
    
    client_info_list *deviceList = NULL;

    if (argc != 3)
    {
        printf("usage:  servername port\n");
        exit(1);
    }
    
    //create one epoll file descriptor
    epollfd = epoll_create(1);
    
    if(epollfd == -1)
    {
        fprintf(stderr, "Create epfd failed: %i\n", errno);
        return(EXIT_FAILURE);
    }
    
    //create the server socket
    srvfd = createSrvSocket(argv[2], epollfd);
    if(srvfd == -1)
    {
        fprintf(stderr, "CreateSrvSocket failed: %i\n", errno);
        return(EXIT_FAILURE);
    }
    
    
    
    //allocate memory for the readBuffer, return if no memory could be allocated
    readBuffer = malloc(BUFFER_LENGTH);
    if(readBuffer == NULL)
    {
        fprintf(stderr, "Malloc failed: %i\n", errno);
        return EXIT_FAILURE;
    }
    
    while(active)
    {
        event_counter = epoll_wait(epollfd, events, MAXEVENTS, 20*1000);

        //iterate over all fired events
        for(i = 0; i < event_counter; i++)
        {
            state_information *state_info = (state_information*)events[i].data.ptr;
            
            if(events[i].events & EPOLLIN)
            {
                //socket indicates data available -> call read handler
                readCallback(epollfd, state_info, readBuffer, &deviceList);                
            }

            if(events[i].events & EPOLLOUT)
            {
                //write was requested and writing is now possible -> call write handler
                writeCallback(epollfd, state_info);
            }

            if (events[i].events & (EPOLLRDHUP | EPOLLHUP))
            {
/*
                closeCallback(state_info->sockfd);
*/
                free(state_info);
            }
            
            //reactivate events, since EPOLLONESHOT disabled it
            setInterestOps(epollfd, (state_information*)events[i].data.ptr);
        }
        
        //is house keeping mandatory?
        if(lastHouseKeeping <= (uint64_t)(time(NULL)-20))
        {
            fprintf(stderr, "HouseKeeping\n");
            
            client_info_list *curEntry = (client_info_list*)deviceList;
            client_info_list *prevEntry = NULL;

            //iterate all registered devices
            while (curEntry != NULL) 
            {
                //check if device is dead
                if (curEntry->clientInfo->lastKeepAlive <= (uint64_t)(time(NULL)-20)) 
                {
                    //delete device
                    if (prevEntry != NULL) 
                    {
                        prevEntry->next = curEntry->next;
                    } 
                    else 
                    {
                        deviceList = (client_info_list*) curEntry->next;
                    }

                    fprintf(stderr, "HouseKeeping: Unregister %s\n", curEntry->clientInfo->name);
                    
                    //tidy up
                    free(curEntry->clientInfo->name);
                    free(curEntry->clientInfo->addr);
                    free(curEntry->clientInfo);
                    //free(curEntry);
                }
                //iterate
                prevEntry = curEntry;
                curEntry = (client_info_list*) curEntry->next;
            }
            lastHouseKeeping = time(NULL);
        }
    }
    free(readBuffer);
}
